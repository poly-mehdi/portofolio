import ProjectsCard from './ProjectsCard'
import { projects, miniProjects } from '../data'
import SectionTitle from './SectionTitle'

const Projects = () => {
  return (
    <section className='bg-portfolio'>
      <div className='py-20 align-element'>
        <h1 className='text-white text-5xl font-bold'>Projects</h1>
        <div className='py-16 grid lg:grid-cols-2 xl:grid-cols-3 gap-8'>
          {projects.map((project) => {
            return <ProjectsCard key={project.id} {...project} />
          })}
        </div>
      </div>
    </section>
  )
}
export default Projects
