import heroImg from '../assets/mehdi-portfolio.png'
import { TypeAnimation } from 'react-type-animation'

const Hero = () => {
  return (
    <div className=' bg-portfolio'>
      <div className='align-element flex flex-col-reverse gap-8 md:gap-0 md:flex-row items-center justify-between border-b-2 pb-20 md:relative md:h-99 md:pt-20 lg:pt-0 lg:h-110 z-10'>
        <article>
          <h1 className='text-3xl text-center md:text-left md:text-6xl md:max-w-80 lg:max-w-max xl:text-7xl font-bold tracking-wider text-white mb-1'>
            Nice to meet you!
          </h1>
          <h1 className='text-2xl text-center md:text-left md:text-4xl lg:text-5xl font-bold tracking-wider text-white z-10 xl:text-6xl'>
            I'm{' '}
            <span className='underline-custom-sm underline-offset-4 md:underline-custom-md lg:underline-offset-8 whitespace-nowrap'>
              <TypeAnimation
                sequence={[
                  'Mehdi Sehad.',
                  2000,
                  'Front-End Developer.',
                  2000,
                  'Full-Stack Developer.',
                  2000,
                  'Tech Enthusiast.',
                  2000,
                  'Mehdi Sehad.',
                  2000,
                ]}
                wrapper='span'
                cursor={true}
                repeat={Infinity}
                style={{ display: 'inline' }}
              />
            </span>
          </h1>
          <p className='text-center md:text-left mt-4 md:text-xl lg:text-3xl text-slate-500 capitalize tracking-wide'>
            Graduate of Polytechnique Montreal
          </p>
          <p className='text-center md:text-left mt-4 md:text-xl lg:text-2xl text-slate-500 capitalize tracking-wide italic '>
            Software Engineering
          </p>
        </article>
        <article className='block md:-z-10 md:absolute right-8 top-0'>
          <img src={heroImg} className='object-cover w-60 lg:w-100 md:w-80' />
        </article>
      </div>
    </div>
  )
}
export default Hero
