import { FaLinkedin, FaGitlab } from 'react-icons/fa'

const Navbar = () => {
  return (
    <nav className='shadow-sm bg-portfolio'>
      <div className='align-element flex flex-col items-center gap-2 md:gap-0 py-4 md:py-0 md:flex-row md:items-center md:justify-between'>
        <div className='flex flex-col sm:flex-row sm:gap-x-16 items-center'>
          <h2 className='text-4xl font-bold  text-white'>mehdisehad</h2>
        </div>

        <div className='flex gap-x-8 pl-8 md:pl-0 md:w-80 lg:w-100 md:bg-portfolioLight md:justify-end md:py-8 pr-8'>
          <a href='https://gitlab.com/poly-mehdi' target='_blank'>
            <FaGitlab className='h-8 w-8 text-slate-300 hover:text-vert duration-300' />
          </a>
          <a href='https://www.linkedin.com/in/mehdi-sehad/' target='_blank'>
            <FaLinkedin className='h-8 w-8 text-slate-200 hover:text-vert duration-300' />
          </a>
        </div>
      </div>
    </nav>
  )
}
export default Navbar
