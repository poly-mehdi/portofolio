import React from 'react'

const ProjectsCard = ({ url, img, github, title, stack }) => {
  return (
    <article className='relative group'>
      <img src={img} alt={title} className='w-full object-cover h-64' />
      <div className='capitalize py-4'>
        <h2 className='text-xl tracking-wide font-bold uppercase text-white'>
          {title}
        </h2>
        <p className='text-gray-400 uppercase tracking-wide font-semibold'>
          {stack}
        </p>
        <div className='mt-2 flex gap-x-4 lg:hidden'>
          {url && (
            <a
              href={url}
              target='_blank'
              className='text-white uppercase font-bold tracking-wider text-l underline-custom-sm underline-offset-8 cursor-pointer'
            >
              View project
            </a>
          )}
          <a
            href={github}
            target='_blank'
            className='text-white uppercase font-bold tracking-wider text-l underline-custom-sm underline-offset-8 cursor-pointer'
          >
            View code
          </a>
        </div>
      </div>
      <div className='absolute inset-0 bg-black bg-opacity-50 hidden text-center text-white opacity-0 group-hover:opacity-100 transition-opacity duration-300 lg:flex lg:flex-col lg:items-center lg:justify-center lg:gap-8 lg:pb-20 '>
        {url && (
          <a
            href={url}
            target='_blank'
            className='text-white uppercase font-bold tracking-wider text-lg underline-custom-sm underline-offset-8 cursor-pointer mb-4'
          >
            View project
          </a>
        )}
        <a
          href={github}
          target='_blank'
          className='text-white uppercase font-bold tracking-wider text-lg underline-custom-sm underline-offset-8 cursor-pointer'
        >
          View code
        </a>
      </div>
    </article>
  )
}

export default ProjectsCard
