import SectionTitle from './SectionTitle'
const About = () => {
  return (
    <section className='bg-white py-20' id='about'>
      <div className='align-element grid lg:grid-cols-2 items-center gap-16'>
        {/* <img
          src={about}
          className='w-80 mx-auto object-contain lg:w-full md:w-96'
        /> */}
        <article>
          <SectionTitle text='A propos' />
          <p className='text-slate-600 mt-8 leading-loose'>
            Passionné par la programmation et la résolution de problèmes, je
            suis diplômé en génie logiciel à Polytechnique Montréal. Mon
            approche axée sur les détails assure la qualité et l'optimisation
            des projets de développement, avec un intérêt particulier pour le
            web et Flutter. En parallèle de mes études, mon engagement en tant
            qu'éducateur sportif a renforcé mes compétences en travail d'équipe
            et en leadership. Mon expérience en génie logiciel comprend des
            projets significatifs, notamment la version 2.0 du tableau de bord
            de Comparastore et l'évolution d'un logiciel pour le jeu de Scrabble
            en ligne, mettant en avant mes compétences de conception et de
            gestion de projet. En dehors du domaine de la programmation, mes
            loisirs incluent la guitare, le piano et le soccer, reflétant ma
            passion pour l'apprentissage continu et la diversité des
            compétences.
          </p>
        </article>
      </div>
    </section>
  )
}
export default About
