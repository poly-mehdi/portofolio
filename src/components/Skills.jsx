import { skills } from '../data'
import SectionTitle from './SectionTitle'
import SkillsCard from './SkillsCard'
const Skills = () => {
  return (
    <div className='bg-portfolio'>
      <section className='py-20 align-element' id='skills'>
        <div className='py-16 grid gap-8 grid-cols-3 md:grid-cols-4 lg:grid-cols-5'>
          {skills.map((skill) => {
            return <SkillsCard key={skill.id} {...skill} />
          })}
        </div>
      </section>
    </div>
  )
}
export default Skills
