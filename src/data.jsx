import { nanoid } from 'nanoid'
import {
  FaHtml5,
  FaReact,
  FaNode,
  FaAngular,
  FaGit,
  FaCss3,
  FaSass,
  FaPython,
} from 'react-icons/fa'
import { SiCplusplus, SiFlutter, SiJira, SiTypescript } from 'react-icons/si'
import { BiLogoPostgresql, BiLogoMongodb } from 'react-icons/bi'

export const links = [
  { id: nanoid(), href: '#home', text: 'accueil' },
  { id: nanoid(), href: '#skills', text: 'Compétences' },
  { id: nanoid(), href: '#about', text: 'À propos' },
  { id: nanoid(), href: '#projects', text: 'Projets' },
]

export const skills = [
  {
    id: nanoid(),
    title: 'HTML',
    icon: <FaHtml5 className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Typescript',
    icon: <SiTypescript className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'React',
    icon: <FaReact className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Node.js',
    icon: <FaNode className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'CSS',
    icon: <FaCss3 className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'SASS',
    icon: <FaSass className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Python',
    icon: <FaPython className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'PostgreSQL',
    icon: <BiLogoPostgresql className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'MongoDB',
    icon: <BiLogoMongodb className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Git',
    icon: <FaGit className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Flutter',
    icon: <SiFlutter className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Angular',
    icon: <FaAngular className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'Jira',
    icon: <SiJira className='h-16 w-16 text-vert' />,
  },
  {
    id: nanoid(),
    title: 'C++',
    icon: <SiCplusplus className='h-16 w-16 text-vert' />,
  },
]

export const projects = [
  {
    id: nanoid(),
    github: 'https://gitlab.com/poly-mehdi/kanban',
    title: 'Kanban App',
    stack: 'Mern Stack',
    url: 'https://kanban-m95s.onrender.com/',
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/kanban-preview.jpg',
  },
  {
    id: nanoid(),
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/audiophile-preview.jpg',
    url: 'https://audiophile-ecommerce-website-2if7.onrender.com',
    github: 'https://gitlab.com/poly-mehdi/audiophile-ecommerce-website',
    title: 'audiophile eCommerce Website',
    stack: 'Mern Stack',
    text: 'Explorez ce site web e-commerce entièrement réactif, conçu pour une boutique de produits audiophiles haut de gamme avec des fonctionnalités de panier et de paiement intégrées.',
  },
  {
    id: nanoid(),
    github: 'https://gitlab.com/poly-mehdi/abalone-ai-agent',
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/abalone.jpg',
    title: 'Intelligent Agent for the Game Abalone',
    stack: 'Python',
    text: "Découvrez un agent intelligent pour le jeu Abalone, développé en Python avec l'algorithme Minimax et l'élagage Alpha-Beta pour une prise de décision optimale.",
  },
]

export const miniProjects = [
  {
    id: nanoid(),
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/desktop-preview.jpg',
    url: 'https://modern-todo-app-poly-mehdi.vercel.app/',
    github: 'https://gitlab.com/poly-mehdi/modern-todo-app',
    title: 'Application TODO Moderne',
    text: 'Découvrez une application TODO classique avec une touche spéciale : un commutateur de thème sombre/clair et la possibilité de réorganiser les tâches par glisser-déposer pour une expérience utilisateur optimale.',
  },
  {
    id: nanoid(),
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/ip-tracker.jpg',
    url: 'https://ip-address-tracker-poly-mehdi.vercel.app/',
    github: 'https://gitlab.com/poly-mehdi/ip-address-tracker',
    title: "Tracker d'adresse IP",
    text: "Explorez cette application de suivi d'adresse IP qui combine deux API distinctes pour fournir des informations précises et détaillées sur les emplacements.",
  },
  {
    id: nanoid(),
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/planet-facts.jpg',
    url: 'https://planet-facts-nine.vercel.app/',
    github: 'https://gitlab.com/poly-mehdi/planet-facts',
    title: 'Faits sur les planètes',
    text: 'Plongez dans notre système solaire avec ce site web de 8 pages, offrant une exploration détaillée et captivante des caractéristiques de chaque planète.',
  },
  {
    id: nanoid(),
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/dictionnary.jpg',
    url: 'https://dictionary-web-app-henna-alpha.vercel.app/',
    github: 'https://gitlab.com/poly-mehdi/dictionary-web-app',
    title: 'Application Web de Dictionnaire',
    text: 'Découvrez un outil de référence en temps réel avec cette application web de dictionnaire, intégrant une API pour des résultats rapides et précis.',
  },
  {
    id: nanoid(),
    img: 'https://portofolio-mehdi.s3.us-east-2.amazonaws.com/images/password-generator.jpg',
    url: 'https://password-geneator.vercel.app/',
    github: 'https://gitlab.com/poly-mehdi/password-geneator',
    title: 'Générateur de mots de passe',
    text: 'Conçu pour simplifier la création de mots de passe, ce générateur utilise HTML, CSS et JS pour offrir des contrôles de formulaire personnalisés et générer des mots de passe aléatoires.',
  },
]
