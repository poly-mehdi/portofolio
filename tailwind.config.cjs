/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        portfolio: '#151515',
        portfolioLight: '#393838',
        vert: '#4EE1A0',
      },
      width: {
        100: '32rem',
      },
      height: {
        99: '26rem',
        100: '40rem',
        110: '42rem',
      },
      fontFamily: {
        sans: ['Space Grotesk', 'sans-serif'],
      },
    },
  },
}
